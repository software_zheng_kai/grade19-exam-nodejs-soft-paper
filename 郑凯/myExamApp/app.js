'ues strict';

let Koa=require('koa');
let static=require('koa-static');
let parser=require('koa-bodyparser');
let controller=require('./controllers');
let tem=require('./templating');

let app=new Koa();

app.use(static(__dirname,'/static'));
app.use(parser());
app.use(controller());
app.use(tem);


let port =3000;
app.listen(port);
console.log(`http://localhost:${port}`);
