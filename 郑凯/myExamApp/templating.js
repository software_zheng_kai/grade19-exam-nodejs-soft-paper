'use strict';

let nunjucks = require('nunjucks');


function temenv(path,opts){
    path=path || 'views';
    opts= opts || {};
    let env={
    }
    let envnext=nunjucks.configure('views',env);
    return envnext;
    
}
module.exports=async(ctx,next)=>{
    let create=temenv();
    ctx.render=function(views,model){
        ctx.body=create.render(views,model)
    }
    await next;
}